<?php $film = $data['film_info'];
      $sceances = $data['film_reservations'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/projet/style/normalize.css">
  <link rel="stylesheet" href="/projet/style/films.css">
  <title><?= $film['nom']?></title>
</head>
<body>

<?php include('views_static/header.php') ?>

  <div class="main_container">

      <ul class="home_links">
        <?php 
            for($i=0;$i<7;$i++){?>
                    <li><a href="#"><?= date("m-d",strtotime("+".$i. " day"))?></a></li>
                    <?php } ?>
      </ul>
  </div>



  <div class="container">

      <div class="film_info">

        <div class="film_header">
          <h1 id="film-tittle"><?=$film['nom']?></h1>
          <img id="background" src="<?=$film['img_url'] ?>" class="header-img">
        </div>


        <div class="info_sup">
          <ul class="infos">
            <li id="date_sortie">Date sortie : <?=$film['date_sortie']?></li>
            <li id="span_r">Realisateur : <?=$film['realisateur']?></li>
            <li id="acteurs">Acteurs : <?=$film['acteurs']?></li>
            <li id="duree"><img class="logo_illu" src="../../asset/clock.png"><?=$film['duree']?> minutes.</li>
          </ul>
          
        </div>



        <p id="synopsis"><?=$film['synopsis']?></p>
        <div class="_reservations">
          <ul class="reservation_ul">
            <?php if(isset($sceances)){
                    foreach($sceances as $sceance){
            
            ?>
              <li><a src="../../asset/billet.png" href="../../reservation/id/<?=$sceance['id_sceance']?>">
              <?=Date('h:i',strtotime($sceance['date_sceance']))?>  
              </a></li>
            <?php }}
            else{
              ?>
              <a>Aucune scéances disponible ! </a>
              <?php
            }
            ?>
            
          </ul>
        </div>




        <div id="ytplayer"></div>
    </div>

</div>

<script>
  // Load the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replace the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
      height: '360',
      width: '640',
      videoId: '<?php echo explode('=',$data['film_info']['annonce_url'])[1] ?>',
    });
  }
</script>



  
</body>
</html>