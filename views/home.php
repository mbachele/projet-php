<?php 
    require('models/home.php');
    $films = fetchAllList();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/projet/style/normalize.css">
  <link rel="stylesheet" href="/projet/style/style.css">
  <title>Cine Club</title>
</head>
<body>
<?php include('views_static/header.php') ?>
<?php
$date1 = date('Y-m-d');
setlocale(LC_TIME, "fr_FR","French");
$date = strftime("%A %d ", strtotime($date1));
?>

  <div class="main_container">

      <ul class="home_links">
        <?php 
            for($i=0;$i<7;$i++){?>
                    <li><a href="#"><?= date("m-d",strtotime("+".$i. " day"))?></a></li>
                    <?php } ?>
      </ul>
  </div>

  <div class="center">

  <ul id="rig">
      <?php foreach($films as $film){ ?>
      <li>
        <a class="rig-cell" href=<?=  "http://".$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX']."/film/id/".$film['id'] ?>>
        <img class="rig-img" src=

        <?php echo $film['img_url'] == "" ? "asset/trex.png" : $film['img_url']; ?>
        
        >
        <span class="rig-overlay"></span>
        <span class="rig-text"><?=$film['nom']?></span>

      </a>
      </li>
    <?php 
    $i++;
     } ?>
      </div>

  
</body>
</html>