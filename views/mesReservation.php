<?php 
      $reservation = $data['reservation'];
	  $film = $data['films'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/projet/style/normalize.css">
  <link rel="stylesheet" href="/projet/style/style.css">
  <link rel="stylesheet" href="/projet/style/table.css">
  <title>Cine Club</title>
</head>
<?php include('views_static/header.php') ?>
<body>




<body>
	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Date</th>
								<th class="column3">Nom du film</th>
								<th class="column4">Duree</th>
								 <th class="column5">Quantité </th>
								<th class="column6">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($film as $res) {?>
								
								<tr>
									<td class="column1"><?= $res['date_sceance']?></td>
									<td class="column3"><?= $res['nom']?></td>
									<td class="column4"><?=$res['duree'] . " min "?></td>
									<td class="column5"><?=$res['date']?></td>
									<?php
										$date = new DateTime($res['date_sceance']);
										$now = new DateTime();
										if($date < $now){?>

											<td class="column6 " style="color:red">Expiré</td>

											<?php }
											else {?>
												<td class="green column6" style="color:green">À venir</td>
											
											<?php }?>

								</tr>
								<?php
								} ?>

								
						</tbody>
					</table>
				</div>
                
			</div>
            
		</div>
	</div>




</body>

  

  
</body>
</html>