<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php


if ($_SESSION["logged_in"] == false || $_SESSION["user"]["admin"] == 0) {
    header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');

} else {

    if(isset($_SESSION['message']) && $_SESSION['message'] != ''){
        print_r($_SESSION['message']);
        $_SESSION['message'] = '';
    }

}
include('views_static/header.php');
$film = $data['film'];
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/projet/style/normalize.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="/projet/style/films.css">
    <link rel="stylesheet" href="/projet/style/admin.css">
    <link rel="stylesheet" href="/projet/style/style.css">




    <title>Admin</title>
</head>

<body style="background-color: #0093E9">






    <div class="main-container" >
        <form name="add" action="models/crud.php" method="post">
        <div class="rendered-form">
    <div class="">
        <h1 access="false" id="control-2251107"><?=$film['nom']?></h1></div>
    <div class="formbuilder-text form-group field-nom">
        <label for="nom" class="formbuilder-text-label">Nom</label>
        <input type="text" class="form-control" name="nom" access="false" value="<?=$film['nom']?>" id="nom">
    </div>
    <div class="formbuilder-text form-group field-genre">
        <label for="genre" class="formbuilder-text-label">Genre</label>
        <input type="text" class="form-control" name="genre" access="false" value="<?=$film['genre']?>" id="genre">
    </div>
    <div class="formbuilder-text form-group field-realisateur">
        <label for="realisateur" class="formbuilder-text-label">Realisateur</label>
        <input type="text" class="form-control" name="realisateur" access="false" value="<?=$film['realisateur']?>" id="realisateur">
    </div>
    <div class="formbuilder-text form-group field-text-1641316681201">
        <label for="text-1641316681201" class="formbuilder-text-label">Acteurs</label>
        <input type="text" class="form-control" name="text-1641316681201" access="false" value="<?=$film['acteurs']?>" id="text-1641316681201">
    </div>
    <div class="formbuilder-textarea form-group field-textarea-1641317173782">
        <label for="textarea-1641317173782" class="formbuilder-textarea-label">Synopsis</label>
        <textarea type="textarea" class="form-control" name="textarea-1641317173782" access="false" id="textarea-1641317173782"><?=$film['synopsis']?></textarea>
    </div>
    <div class="formbuilder-text form-group field-duree">
        <label for="duree" class="formbuilder-text-label">Duree</label>
        <input type="text" class="form-control" name="duree" access="true" value="<?=$film['duree']?>" id="duree">
    </div>
    <div class="formbuilder-text form-group field-url_img">
        <label for="url_img" class="formbuilder-text-label">Url Img
            <br>
        </label>
        <input type="text" class="form-control" name="url_img" access="true" value="<?=$film['img_url']?>" id="url_img">
    </div>
    <div class="formbuilder-text form-group field-annonce_url">
        <label for="annonce_url" class="formbuilder-text-label">Lien youtube
            <br>
        </label>
        <input type="text" class="form-control" name="annonce_url" access="true" value="<?=$film['annonce_url']?>" id="annonce_url">
    </div>
    <div class="formbuilder-button form-group field-valider">
        <button type="submit" class="btn btn-sucess" name="type" value="updateFilm"  access="false" id="valider">Valider les changements
            <br>
        </button>
    </div>
</div>
        </form>
    
    </div>


</body>
