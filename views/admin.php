<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="/projet/style/normalize.css">
    <link rel="stylesheet" href="/projet/style/films.css">
    <link rel="stylesheet" href="/projet/style/admin.css">
  <link rel="stylesheet" href="/projet/style/table.css">
  


    <title>Admin</title>
</head>

<body>
    <script type="text/javascript" src="javascript/dropdown.js"></script>
    <script type="text/javascript" src="javascript/addform.js"></script>


    <?php

    if ($_SESSION["logged_in"] == false || $_SESSION["user"]["admin"] == 0) {
        header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');

    } else {
        $films = $data['films']->films;
        $reservations = $data['films']->reservations;
        // print_r($reservations);
        //print_r($films);
        if(isset($_SESSION['message']) && $_SESSION['message'] != ''){
            print_r($_SESSION['message']);
            $_SESSION['message'] = '';
        }

    }
    include('views_static/header.php');

    
    ?>




    <div class="main-container">
        <ul class="optionsList">
        <li><a onclick="hideAndShow('addFilm')" class="title"  href="#">Ajouter un film</a></li>
            
            <div style="display:none" id="addFilm">
                <div name="center-div">

                <form name="add" action="models/crud.php" method="post">
                <label for="name">Nom du film : </label>
                <input name="nom" type="text" id="name" class="input">
                <br>
                <label for="type">Genre du film : </label>
                <input name="genre" type="text" id="type" class="input">
                <br>
                <label for="date">Date de sortie du film : </label>
                <input name="date_sortie" type="date" id="date" class="input">
                <br>
                <label for="realisator">Realisateur : </label>
                <input name="realisateur" type="text" id="realisator" class="input">
                <br>
                <label for="actors">Acteurs : </label>
                <input name="acteurs" type="text" id="actors" class="input">
                <br>
                <label for="time">Duree en minutes du film : </label>
                <input name="duree" type="number" id="time" class="input"> 
                <br>
                <label for="synopsis">Synopsis du film : </label>
                <input name="synopsis" type="text" id="synopsis" class="input">
                <br>
                <label for="img_url">Url de l'image : </label>
                <input name="img_url" type="text" id="img_url" class="input">
                <br>
                <label for="video_url">Url de la bande d'annonce : </label>
                <input name="annonce_url" type="text" id="video_url" class="input">
                <br>
                <button name="type" value="addFilm" type="submit" class="btn-grad">Valider</button>
                </form>
                </div>
            </div>
            <li><a onclick="hideAndShow('updateFilm')" class="title" href="#">Modifier un film</a></li>
            <div id="updateFilm" style="display:none">
                <form name="updateFilm" action="utils/redirect.php"  method="get">
                    <select  name="films">
                    <option >--Sélectionner un film--</option>
                    <?php
                        foreach ($films as $f) {?>
                                
                                <option name="<?=$f['id']?>" value=<?=$f['id']?>> <?=$f['nom']?>
                        <?php } ?>
                        
                </select>

                <button name="type" type="submit"  value="updateFilm" class=" ">Modifier</button>

                
                </form>
            
            </div>
            <li><a onclick="hideAndShow('viewResa')" class="title" href="#">Voir les réservations</a></li>
            <div style="display:none" id="viewResa" class="limiter">
            <div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Date</th>
								<th class="column3">Film ID</th>
								<th class="column4">Prix</th>
								<th class="column5">User ID</th>
							</tr>
						</thead>
						<tbody>
                            <?php foreach ($reservations as $res) {?>

                            
								<tr>
									<td class="column1"><?= $res['date_sceance']?></td>
									<td class="column3"><?= $res['id_film']?></td>
									<td class="column4"><?=$res['prix']. "  €"?></td>
									<td class="column5"><?=4?></td>
								</tr>

                                <?php }?>
								
						</tbody>
					</table>
				</div>
                
			</div>
            
		</div>
            </div>
            <li><a onclick="hideAndShow('addSceance')" class="title" href="#">Ajouter un créneau</a></li>
            <div style="display:none" id="addSceance">
                <form name="addSceance" action="models/crud.php"  method="post">
                    <select  name="films">
                        <option >--Sélectionner un film--</option>
                        <?php
                        foreach ($films as $f) {?>

                        <option name="<?=$f['id']?>" value=<?=$f['id']?>> <?=$f['nom']?>
                            <?php } ?>

                    </select>
                    </br>

                    <label for="meeting-time">Choose a time for your appointment:</label>
                    </br>

                    <?php
                    $mindate = date("Y-m-d");
                    $mintime = date("h:i");
                    $min = $mindate."T".$mintime;
                    $maxdate = date("Y-m-d", strtotime("+30 Days"));
                    $maxtime = date("h:i");
                    $max = $maxdate."T".$maxtime;
                    ?>
                    <input type="datetime-local" class="form-control" id="time" name="time" min="<?php echo $min?>" max="<?php echo $max?>" required>
                    <br>
                    <label for="time">Prix </label>
                    <input name="price" type="number" id="time" class="input">
                    <br>

                    <button name="type" type="submit" value="addSceance" class="btn">Ajouter un créneau</button>


                </form>
            </div>



        </ul>

    </div>


</body>
