
<?php
  if($_SESSION["logged_in"] == true){
    header("Location: http://localhost/projet/home");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Register</title>
</head>
<body>
  <!-- Logo du site -->
  <div class="container pt-8">


  </div>

  <!-- Container de form pour compte -->
    <div class="container center p-4 d-flex justify-content-center">
      
    <form action="models/register.php" method="post" >
      <img src="asset/logo.png" alt="cineclub">
        <div class="mb-3">
          <label for="username" class="form-label">Username</label>
          <input name="username" type="text" class="form-control" id="username">
        </div>

        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input name="email" type="email" class="form-control" id="email">
        </div>

        <div class="mb-3">
          <label for="password" class="form-label" >Password</label>
          <input type="password" class="form-control" id="password" name="password">
        </div>
        
        <div class="mb-3">
          <label for="password-confirmation" class="form-label" >Password-confirmation</label>
          <input type="password" class="form-control" id="password-confirmation" name="password-confirmation">
        </div>

        <!-- Checkbox se souvenir de moi : applique un cookie pour se souvenir de l'utilisateur -->
        <div class="mb-3 form-check">
          <input type="checkbox" class="form-check-input" id="checkbox_save">
          <label class="form-check-label" for="checkbox_save">Se souvenir de moi</label>
        </div>
        <div class="row">
        <button type="submit" class="btn btn-primary">S'inscrire</button>

        <?php
        // Affiche un message d'erreur si il y'en a un.
        if(isset($_SESSION['error'])){
          `<div class="alert alert-danger" role="alert">` .
          $_SESSION['error']
          . ` </div>`;
          unset($_SESSION['error']);
        };
        ?>
        
        <a href="https://localhost/projet/login" class="mt-1 mb-1 text-muted text-center">Se connecter</a>

        </div>
      </form>
      
    </div>

</body>
</html>