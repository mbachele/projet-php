
<?php

  if(isset($_SESSION["logged_in"])){
    if( $_SESSION["logged_in"] == true){
      header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');
    }
  }

  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Home</title>
</head>
<body>
<div class="container pt-8">


</div>

<!-- Container de form pour compte -->
  <div class="container center p-4 d-flex justify-content-center">
    
  <form action="models/login.php" method="post" >
    <img src="asset/logo.png" alt="cineclub">
      <div class="mb-3">
        <label for="username" class="form-label">Username</label>
        <input name="username" type="text" class="form-control" id="username">
      </div>

      <div class="mb-3">
        <label for="password" class="form-label" >Password</label>
        <input type="password" class="form-control" id="password" name="password">
      </div>

      <!-- Checkbox se souvenir de moi : applique un cookie pour se souvenir de l'utilisateur -->
      <div class="mb-3 form-check">
        <input type="checkbox" class="form-check-input" id="checkbox_save">
        <label class="form-check-label" for="checkbox_save">Se souvenir de moi</label>
      </div>
      <div class="row">
      <button type="submit" class="btn btn-primary">Se connecter</button>

      <?php
      // Affiche un message d'erreur si il y'en a un.
      if(isset($_SESSION['error'])){
        echo `<div class="alert alert-danger" role="alert">` .
        $_SESSION['error']
        . ` </div>`;
        unset($_SESSION['error']);
      };
      ?>
      
      <a href="https://localhost/projet/register" class="mt-1 mb-1 text-muted text-center">S'inscrire</a>

      </div>
    </form>
    
  </div>

</body>
</html>
