-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 04, 2022 at 03:15 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projet`
--

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `id` int(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `genre` varchar(128) NOT NULL,
  `date_sortie` varchar(128) NOT NULL,
  `realisateur` varchar(128) NOT NULL,
  `acteurs` varchar(256) NOT NULL,
  `synopsis` text NOT NULL,
  `duree` int(128) NOT NULL,
  `img_url` varchar(128) DEFAULT NULL,
  `annonce_url` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `nom`, `genre`, `date_sortie`, `realisateur`, `acteurs`, `synopsis`, `duree`, `img_url`, `annonce_url`) VALUES
(1, 'Barbaque', 'Comédie', '15/12/2021', 'Alexandra Leclere', 'Josiane Balasko, Didier Bourdon, Marilou Berry, Ben, Laurent Stocker', 'Chantal et Christian vivent une retraite paisible. Mais depuis que leurs enfants Sandrine et Stéphane ont quitté le nid, ceux-ci ne donnent plus beaucoup de nouvelles. Les occasions de se réunir en famille se font de plus en plus rares... Quand les rejetons annoncent qu\'ils ne viendront pas fêter Noël, c\'en est trop pour Chantal et Christian ! Ils décident alors de leur faire croire qu\'ils ont touché le jackpot Une tentative désespérée pour tenter de les faire revenir et un mensonge qui pourrait leur coûter cher…', 92, 'https://fr.web.img6.acsta.net/c_224_335_50_50/pictures/21/10/14/15/21/0862311.jpg', 'https://www.youtube.com/watch?v=PL5r5DTeWjo'),
(2, 'Affamés', ' Epouvante-horreur, thriller', '17/11/2021', 'Scott Cooper', ' Keri Russell, Jesse Plemons, Jeremy T. Thomas, Scott Haze, Rory Cochrane', 'Dans une petite ville minière de l\'Oregon, une institutrice et son frère policier enquêtent sur un jeune écolier. Les secrets de ce dernier vont entraîner d\'effrayantes conséquences.', 99, 'https://fr.web.img4.acsta.net/c_224_335_50_50/pictures/21/09/20/12/25/4274913.jpg', 'https://www.youtube.com/watch?v=qA7fltyrWKY'),
(3, 'Aline', 'Biopic, drame', '10/11/2021', 'Valérie Lemercier', 'Valérie Lemercier, Sylvain Marcel, Danielle Fichaud, Roc Lafortune, Antoine Vézina', 'Le film est présenté hors-compétition au Festival de Cannes 2021.Québec, fin des années 60, Sylvette et Anglomard accueillent leur 14ème enfant : Aline. Dans la famille Dieu, la musique est reine et quand Aline grandit on lui découvre un don, elle a une voix en or. Lorsqu\'il entend cette voix, le producteur de musique Guy-Claude n\'a plus qu\'une idée en tête… faire d\'Aline la plus grande chanteuse au monde. Epaulée par sa famille et guidée par l\'expérience puis l\'amour naissant de Guy-Claude, ils vont ensemble écrire les pages d\'un destin hors du commun.', 126, 'https://fr.web.img3.acsta.net/c_224_335_50_50/pictures/21/06/15/16/11/1152680.jpg', NULL),
(4, '', 'Comédie', '03/11/2021\r\n\r\n', 'Fabrice Maruca', 'Jérémy Lopez, Alice Pol, Artus, Clovis Cornillac, Chantal Neuwirth', 'Quiévrechain, ville industrielle du nord de la France. Après la fermeture de leur usine, Franck, passionné de variété française décide d\'entraîner ses anciens collègues, Sophie (dont il est secrètement amoureux), José (qui chante comme une casserole), et Jean-Claude (ancien cadre un peu trop fier) dans un projet un peu fou : monter une entreprise de livraisons de chansons à domicile, SI ON CHANTAIT ! Départs en retraite, anniversaires ; à force de débrouille, ils commencent à avoir de plus en plus de demandes. Mais entre chansons, tensions et problèmes de livraisons, les fausses notes vont être dures à éviter !', 0, 'https://fr.web.img4.acsta.net/c_224_335_50_50/pictures/21/09/24/11/11/1430693.jpg', NULL),
(5, 'Barbaque', 'Comédie', '27/10/2021', 'Fabrice Eboué', 'Fabrice Eboué, Marina Foïs, Nicolas Lumbreras, Victor Meutelet, Lisa Do Couto Texeira', 'Vincent et Sophie sont bouchers. Leur commerce, tout comme leur couple, est en crise. Mais leur vie va basculer le jour où Vincent tue accidentellement un vegan militant qui a saccagé leur boutique… Pour se débarrasser du corps, il en fait un jambon que sa femme va vendre par mégarde. Jamais jambon n\'avait connu un tel succès ! L\'idée de recommencer pourrait bien les titiller…', 92, 'https://fr.web.img6.acsta.net/c_224_335_50_50/pictures/21/09/09/15/43/3782383.jpg', NULL),
(6, 'Last Night in Soho', 'Epouvante-horreur, thriller', '27/10/2021', 'Edgar Wright', 'Thomasin McKenzie, Anya Taylor-Joy, Matt Smith (XI), Diana Rigg, Terence Stamp', 'LAST NIGHT IN SOHO met en scène l\'histoire d\'une jeune femme passionnée de mode et de design qui parvient mystérieusement à retourner dans les années 60 où elle rencontre son idole, une éblouissante jeune star montante. Mais le Londres des années 60 n\'est pas ce qu\'il parait, et le temps semble se désagréger entrainant de sombres répercussions.', 127, 'https://fr.web.img6.acsta.net/c_224_335_50_50/pictures/21/09/20/15/10/1064328.jpg', NULL),
(8, 'fzef', 'ezfze', '', 'ezf', 'zefzef', 'fze', 656, 'fze', 'fzefze'),
(9, 'azd', 'd', '2020-11-29', 'de', 'f', '', 6, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id_reservation` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sceance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id_reservation`, `id_user`, `id_sceance`) VALUES
(1, 4, 4),
(2, 5, 8),
(3, 5, 3),
(4, 5, 3),
(5, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sceances`
--

CREATE TABLE `sceances` (
  `id_sceance` int(11) NOT NULL,
  `id_film` int(11) NOT NULL,
  `date_sceance` datetime NOT NULL,
  `prix` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sceances`
--

INSERT INTO `sceances` (`id_sceance`, `id_film`, `date_sceance`, `prix`) VALUES
(1, 1, '2021-11-15 22:22:36', 14),
(2, 1, '2021-11-15 23:22:36', 14),
(3, 1, '2021-11-15 22:22:36', 14),
(4, 1, '2021-11-15 18:22:36', 14),
(5, 1, '2021-11-15 17:22:36', 14),
(6, 1, '2021-11-15 22:22:36', 14),
(7, 1, '2021-11-15 22:22:36', 14),
(8, 1, '2021-11-15 13:22:36', 14);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(60) NOT NULL,
  `reservation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`reservation`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `admin`, `password`, `reservation`) VALUES
(1, 'fzefzef', 'fzefzefze', 0, 'fzefzefzefze', NULL),
(2, 'fzefzef', 'fzefzefze', 0, 'fzefzefzefze', NULL),
(3, '', '', 0, '$2y$10$IJj.gYcHPy7tHpMB0fWaHOqq/eAx.DyY3hrmI8wCQOcVc2A2jHK6q', NULL),
(4, 'a', 'a@e.fr', 1, '$2y$10$sj3Pa1s9iKNV0H2mSQyZJufR7MbvBsE.nyeadIMTTcpMBJu/uR.KO', NULL),
(5, 'f', 'fournixx@gmail.com', 0, '$2y$10$Wj83bwgeW.20iW7o5np/0.X1.VmH4LQ.rhX1Yl3/.Qfd6c12YDN0W', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `titre` (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id_reservation`);

--
-- Indexes for table `sceances`
--
ALTER TABLE `sceances`
  ADD PRIMARY KEY (`id_sceance`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id_reservation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sceances`
--
ALTER TABLE `sceances`
  MODIFY `id_sceance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
