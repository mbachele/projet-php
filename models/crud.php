<?php
session_start();
require_once('Database.php');
$database = new Database();

if(isset($_POST['type'])){

    switch($_POST['type']){
        case 'addFilm':
            addFilm($database->db);
            break;
        case 'updateFilm':
            updateFilm($database->db);
            break;
        case 'addSceance':
            addSceance($database->db);
            break;
            

    }
    $_SESSION['message'] = $_POST;
    header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/admin');

}
else {
    $_SESSION['message'] = "Verifiez les informations rentré.";
    header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/admin');
}

function addSceance($pdo){
    $sql = "INSERT INTO `sceances`(`id_sceance`, `id_film`, `date_sceance`, `prix`) VALUES (NULL, ?,?,?)";

    $req = $pdo->prepare($sql);
    $req->bindParam(1,$_POST['films']);
    $req->bindParam(2,$_POST['time']);
    $req->bindParam(3,$_POST['price']);
    $req->execute();


}

function updateFilm($pdo){

    $sql = "UPDATE `films` SET `id`= :id,`nom`= :nom ,`genre`= :genre ,`date_sortie`= :date_sortie,
    `realisateur`= :realisateur ,`acteurs`= :acteurs,`synopsis`= :synopsis,`duree`= :duree,
    `img_url`= :img_url,`annonce_url`= :annonce_url WHERE 1";
    $req = $pdo->prepare($sql);
    
    foreach ($_POST as $key => &$val) {
        $req->bindParam($key, htmlspecialchars($val));
    }

    return $req->execute();
}

function addFilm($pdo){
    $sql = "INSERT INTO `films`(
        `id`,
        `nom`,
        `genre`,
        `date_sortie`,
        `realisateur`,
        `acteurs`,
        `synopsis`,
        `duree`,
        `img_url`,
        `annonce_url`
    )
    VALUES(
        NULL,:nom,:genre,:date_sortie,:realisateur,:acteurs,:synopsis,:duree,:img_url,:annonce_url);";
    $req = $pdo->prepare($sql);
    unset($_POST['type']);
    
    foreach ($_POST as $key => &$val) {
        $req->bindParam($key, htmlspecialchars($val));
    }

    return $req->execute();
}

?>