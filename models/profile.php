<?php
    require_once('Database.php');

    class Profile
    {

        public $reservation;
        public $films;
        private $database;
        
        function __construct()
        {   $this->database = new Database();
            $this->reservation = $this->fetchUserReservation($this->database->db);
            $this->films = $this->fetchUserFilm($this->database->db);

        }
        

        public function fetchFilm($pdo,$id){

            $sql = "SELECT * from films where id = ?";
            $req = $pdo->prepare($sql);
            $req->bindParam(1, $this->sceance['id_film']);
            $req->execute();
            $film = $req->fetch();
            return $film;
        }



        public function fetchUserReservation($pdo){
            
            $sql = "SELECT * FROM `sceances` WHERE id_sceance IN ( SELECT id_sceance from reservations where id_user = :id ) ORDER BY date_sceance";
            $req = $pdo->prepare($sql);
            $id = $_SESSION['user']['id'];
            $req->bindParam('id',$id);
            $req->execute();
            $reservation = $req->fetchAll();
            return $reservation;
        }

        public function fetchUserFilm($pdo){
            
            $sql = "SELECT F.nom, F.duree, count(S.date_sceance) as date, S.date_sceance as date_sceance FROM reservations R JOIN sceances S on R.id_sceance = S.id_sceance JOIN films F on S.id_film = F.id WHERE R.id_user = :id group by date_sceance;";
            $req = $pdo->prepare($sql);
            $id = $_SESSION['user']['id'];
            $req->bindParam('id',$id);
            $req->execute();
            $reservation = $req->fetchAll();
            return $reservation;
        }

    }

?>