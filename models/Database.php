<?php

class Database{
   
    public $db;

    public function __construct()
    {
        $this->db = $this->connect();
    }

    public function connect(){

        $host = '127.0.0.1';
        $db   = 'projet';
        $user = 'root';
        $pass = '';
        $charset = 'utf8';
        
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {

            $pdo = new PDO($dsn, $user, $pass, $options);
            return $pdo;


        } catch (\PDOException $e) {
            echo "Db is broken";
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

}