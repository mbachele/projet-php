<?php
require_once('Database.php');


class AdminPanel
{

    public $films;
    public $selectedFilm;
    public $reservations;
    private $database;

    function __construct($id)
    {   $this->database = new Database();
        $this->films = $this->fetchAllFilms($this->database->db);
        $this->reservations = $this->fetchAllReservation($this->database->db);

        if(isset($id) && $id > 0){
            $this->selectedFilm = $this->fetchFilmById($this->database->db,$id);
        }
    }
    public function fetchAllFilms($pdo){

        $sql = "SELECT * from films ";
        $req = $pdo->prepare($sql);
        $req->execute();
        return $req->fetchAll();

    }
    public function fetchFilm($pdo,$id){

        $sql = "SELECT * from films where id = ?";
        $req = $pdo->prepare($sql);
        $req->bindParam(1, $id);
        $req->execute();
        $film = $req->fetch();
        if($film == false){
            header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');

        }
        else{

            return $film;
        }
    }
    public function fetchAllReservation($pdo){
        $sql = "SELECT * FROM `sceances` ORDER BY date_sceance";
        $req = $pdo->prepare($sql);
        $req->execute();
        $reservation = $req->fetchAll();
        if($reservation == false){
            return null;
        }
        else{

            return $reservation;
        }
    }

    public function fetchReservation($pdo,$id){

        $sql = "SELECT * FROM `sceances` WHERE id_film= ? ORDER BY date_sceance";
        $req = $pdo->prepare($sql);
        $req->bindParam(1, $id);
        $req->execute();
        $reservation = $req->fetchAll();
        if($reservation == false){
            $reservation['message'] = 'Aucun créneau pour aujourd\'hui.';
        }
        else{

            return $reservation;
        }
    }

    public function fetchFilmById($pdo,$id){

        $sql = "SELECT * from films where id = ?";
        $req = $pdo->prepare($sql);
        $req->bindParam(1, $id);
        $req->execute();
        $film = $req->fetch();
        if($film == false){
            header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/admin');
        }
        else{
                
            return $film;
        }
    }

    public function formatDate($date){


    }
}

?>