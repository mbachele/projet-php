<?php
    require_once('Database.php');

    class Films
    {

        public $film;
        public $reservations;
        private $database;
        
        function __construct($id)
        {   $this->database = new Database();
            $this->film = $this->fetchFilm($this->database->db,$id);
            $this->reservations = $this->fetchReservation($this->database->db,$id);
        }
        public function fetchAll($pdo){

            $sql = "SELECT * from films ";
            $req = $pdo->prepare($sql);
            $req->execute();
            $film = $req->fetchAll();
            return $film;

        }
        public function fetchFilm($pdo,$id){

            $sql = "SELECT * from films where id = ?";
            $req = $pdo->prepare($sql);
            $req->bindParam(1, $id);
            $req->execute();
            $film = $req->fetch();
            if($film == false){
                header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');
            }
            else{
                    
                return $film;
            }
        }
        public function fetchReservation($pdo,$id){
            
            $sql = "SELECT * FROM `sceances` WHERE id_film= ? AND date_sceance > NOW() ORDER BY date_sceance";
            $req = $pdo->prepare($sql);
            $req->bindParam(1, $id);
            $req->execute();
            $reservation = $req->fetchAll();
            if($reservation == false){
                $reservation['message'] = 'Aucun créneau pour aujourd\'hui.';
            }
            else{
                
                return $reservation;
            }
        }

        public function formatDate($date){


        }
    }

?>