<?php
    require_once('Database.php');

    class Reservations
    {

        public $film;
        public $sceance;
        private $database;

        function __construct($id)
        {   $this->database = new Database();
            $this->sceance = $this->fetchReservation($this->database->db,$id);
            $this->film = $this->fetchFilm($this->database->db,$id);

        }
        

        public function fetchFilm($pdo,$id){

            $sql = "SELECT * from films where id = ?";
            $req = $pdo->prepare($sql);
            $req->bindParam(1, $this->sceance['id_film']);
            $req->execute();
            $film = $req->fetch();

            // if($film == false){
            //     header("Location: http://localhost/projet/home");
            // }
            // else{
                    
            //     return $film;
            // }
            return $film;
        }

        public function newReservation(){
            if($_SESSION['logged_in'] == 1 ){
                $sql = "INSERT INTO `reservations`(`id_reservation`, `id_user`, `id_sceance`) VALUES (NULL,?,?)";
                $req = $this->database->db->prepare($sql);
                $req->bindParam(1, $_SESSION['user']['id']);
                $req->bindParam(2, $this->sceance['id_sceance']);
                $req->execute();
                print_r($req);
            }
            else{

                $_SESSION['temp_resa'] = $this->sceance;
                header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/login');

            }

        }

        public function fetchReservation($pdo,$id){
            
            $sql = "SELECT * FROM `sceances` WHERE id_sceance = ? ORDER BY date_sceance";
            $req = $pdo->prepare($sql);
            $req->bindParam(1, $id);
            $req->execute();
            $reservation = $req->fetch();
            return $reservation;
        }

        public function fetchUserReservation($pdo,$id){
            
            $sql = "SELECT * FROM `sceances` WHERE id_sceance = ( SELECT id_sceance from reservations where id_user = ?) ORDER BY date_sceance";
            $req = $pdo->prepare($sql);
            $req->bindParam(1, $_SESSION['user']['user_id']);
            $req->execute();
            $reservation = $req->fetchAll();
            return $reservation;
        }


        public function valider(){

            session_start();

            $sql = "INSERT INTO `reservations`(`id_reservation`, `id_user`, `id_sceance`, `prix`) VALUES (' ?, ?, ?, ?')";

            $req = $this->database->db->prepare($sql);
            
            $req->bindParam(1, $id);
            $req->bindParam(1, $id);
            $req->bindParam(1, $id);
            $req->execute();
            header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');

        }
    }

?>