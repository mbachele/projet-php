

N° | Tâche | Priorité | Note | État  
-----  | -------------  | - | ----- | ---
1 | Installations des outils | 2 | | [ X ]
2 | Création de la base de donnée utilisateur | 2 | | [ X ]
3 | Création du projet git | 2 | | [ X ]
4 | Création des dossiers de base | 2 | | [ X ]
5 | Création des fichier de base | 2 | | [ X ]
6 | Connection à la base de donnée | 2 | | [ X ]
7 | Création d'une page de connection | 2 | | [ X ]
8 | Création d'une page d'inscription | 2 | | [ X ]
9 | Enregistrer la session user et le ramener sur la homepage quand il est log | 2 | | [ X ]
8 | Afficher à l'écran les erreur de connection / création de compte | 1 | | [ X ]
9 | Creation des requetes pour se connecter et s'inscrire | 2 | | [ X ]
10 | Créer un attribut admin et le sécuriser. | 1 | | [ X ]
11 | Créer la base de données pour les films.  | 1 | | [ X ]
12 | Les films doivent avoir : Une durée, un nom, une liste d'acteur, le nombres de spectateur maximum.  | 1 | | [ X ]
13 | Créer la page d'acceuil pour présenter les films à l'affiche | 1 | | [ X ]
14 | L'utilisateur doit pouvoir s'enregistrer pour regarder le film | 1 | | [Xcs ]
15 | Une redirection lui permettera de sélectionner son siège | 1 | | [X ]
16 | Une fois son siège valider l'utilisateur est redirigé vers une page de confirmation | 1 | | [ X] 
17 | La base de donnée doit enregistrer l'id de l'utilisateur qui participé | 1 | | [X ]
18 | La page de confirmation reste affiché pendant 2 sec pour l'utilisateur est redirigé vers l'acceuil | 1 | | [ ]
19 | Faire en sorte que l'utilisateur voi directement les films pour lequel il a reservé. | 1 | | [ ]
20 | Le panel admin doit être visible seulement pour les administrateur | 1 | | [ X]
21 | Il retrouve le nombre de spectateur, ( la répartition des sièges ? ) | 1 | | [ X]
22 | Les admins peuvent créer un poste. | 1 | | [ ]X
23 | Les admins peuvent supprimer un poste. | 1 | | [X ]

Modifier un film
Supprimer un film
ajouter un créneaux


