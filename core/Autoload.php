<?php

class AutoLoad {

    public static function start() {
        spl_autoload_register(array(__CLASS__, 'load'));

        $root = $_SERVER['DOCUMENT_ROOT'];
        $host = $_SERVER['HTTP_HOST'];

        define('HOST', 'http://'.$host.'/');
        define('ROOT', '/home/bash/php/projet-php/');

        define('CONTROLLERS', ROOT.'controllers/');
        define('MODELS', ROOT.'models/');
        define('VIEWS', ROOT.'views/');
        define('UTILS', ROOT.'utils/');

    }

    public static function load($class) {
        if (file_exists(MODELS.$class.'.php')) {
            include_once(MODELS.$class.'.php');
        } elseif (file_exists(UTILS.$class.'.php')) {
            include_once(UTILS.$class.'.php');
        } elseif (file_exists(CONTROLLERS.$class.'.php')) {
            include_once(CONTROLLERS.$class.'.php');
        }
    }
}
