<?php


    class App{
        /**
         * 
         */
        protected $controller = 'home';
        protected $method = 'index';
        protected $params = [];
        protected $db;
        function __construct()
        {

            $url = $this->parseUrl();
            // If controller exist then instanciate it.
             
            if(file_exists('controllers/' . $url[0] .'.php'))
            {

                $this->controller = $url[0];
                unset($url[0]);
            }  
            
            require_once('controllers/' . $this->controller . '.php');
            
            $this->controller = new $this->controller;
            
            if (isset($url[1])){

                if(method_exists($this->controller, $url[1]))
                {
                    
                    $this->method = $url[1];
                    unset($url[1]);
                }
            }

            // If params remains after parsing Controller/methods params are define else [ ]
            $this->params = $url ? array_values($url) : [];

            call_user_func_array([$this->controller,$this->method], $this->params);


        }
        // Transform url into an array of params

        function parseUrl(){
            if(isset($_GET['url'])){
                return $url = explode('/',filter_var(rtrim($_GET['url'], '/'),FILTER_SANITIZE_URL));
            }
        }
    }
?>