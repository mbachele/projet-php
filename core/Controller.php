<?php


class Controller{

    public function model($model,$args=''){
        require_once('models/' . $model . '.php');
        return new $model($args);
    }

    public function view($view, $data = []){
        require_once('views/' . $view . '.php');
    }


}

?>