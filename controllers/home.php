<?php


class Home extends Controller{

        public function index(){
            //$user = $this->model('User');
            $this->view('home');
        }

        public function profile(){
            if(!isset($_SESSION['logged_in'])){
                header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['CONTEXT_PREFIX'].'/home');

            }
            $sceance = $this->model('profile');
            $this->view('mesReservation',['reservation' => $sceance->reservation,'films' => $sceance->films]);
        }

}