<?php


class Film extends Controller{

        public function index($id=''){

            $film = $this->model('films');
            $film->id =$id;
            $this->view('film',['film_id' => $film->id]);
        }

        public function id($id=''){

            $film = $this->model('films',$id);
            $this->view('film',['film_info' => $film->film,'film_reservations' => $film->reservations]);
    }

}