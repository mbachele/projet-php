<?php


class Admin extends Controller{


    public function index(){

        $films = $this->model('adminPanel');
        $this->view('admin',['films' => $films]);
    }

    public function update($id=''){
        $films = $this->model('adminPanel',$id);
        $this->view('admin_update',['film' => $films->selectedFilm]);

        
    }

}